# motm - the messageboard on the moon 
a flexible messageboard for the gemini protocol

live @ gemini://illegaldrugs.net/cgi-bin/motm

### features
- account registration with client certs
- unlimited length post creation
- basic moderation
- bans and unbans
- customizable templates for each page
- flexible messageboard creation

### prerequisites
- python >= 3.7.3
- cryptography >= 3.1
- sqlite3
- a gemini server fully compatible with jetforce's cgi implementation

### using motm (end user)
- **navigation**
    - navigating motm is fairly straightforward, select a board to view its
      posts, select a post to view its replies.
- **registration**
    - if you try to post to a board you will get an error
	- this is because you are not registered
  - registration is a simple process, but it is easy to mess up and lose your
  username, read this fully before attempting to register
  - in order to register, go to gemini://[path to motm]/register, e.g.
  gemini://illegaldrugs.net/cgi-bin/motm/register for the messageboard
  hosted at gemini://illegaldrugs.net/cgi-bin/motm
  - you will be prompted for a username
  - input your username
  - if it is not taken, you will be given a client cert, as well as a key
	  - **DO NOT SHARE THIS KEY.**
  - save this page, it will not be regenerated
    - copy the key and cert into separate files, you should probably name them
      something like "[username].cert" and "[username].key" respectively
    - now, restart your client, this time supplying the respective certificate
      and key files
	  - for example, with av98 i would run `av98 --tls-cert [username].cert
	  --tls-key [username].key`
- **posting**
    - once you have registered, you can create a new post by selecting the
      'Create new post' option 
    - input the title you would like to use for your post
    - now the post has been created
    - in order to add text to the post, navigate to it and select the 'Append'
      option
    - you may repeat this process as much as you like, the post is automatically
      updated on each append
    - no spaces or newlines are added for you when appending to a post, however
      a newline can be inserted by using the string "\n" in your text 

### hosting motm
- make sure you are not root
- clone this repo somewhere onto your computer
- open the 'config/settings.json' file
    - edit the `hostname` entry in the `server` section to reflect the hostname
      or ip of your server
    - edit the names and settings for the **3** default boards, under
      `defaultPosts` adding more boards if you like
	- `motm` is not a board, leave that alone
- run `./cli.py` once to generate the necessary files
    - once you're done, `^C` to exit
- copy the 'motm.cgi' file to your cgi-bin
- edit the 'motm.cgi' file so that it points to the repo you cloned earlier
- start your server, making sure to specify the newly created 'certs/ca.cert'
  as your cafile

### moderating motm
- moderating your messageboard is done through the 'cli.py' script
- motm provides a few commands for moderation
  - `del_post [post id]`
	  - deletes a post
  - `ban [username]`
	  - bans a user
  - `unban [username]`
	  - unbans a user

### customizing motm
- basic customization is done by editing the templates in the 'config/templates'
  dir
- further customization can be done by editing the settings.json file, though
  this file is fragile and improper editing can easily break the script

### notes/bugs
- this software was tested with
  [jetforce's](https://github.com/michael-lazar/jetforce) cgi implementation, it
should work with other servers provided they implement the necessary cgi vars for
verifying tls certificates
- password protected cas are not supported right now, if you create your ca with
  a password motm will break
- the scoring system might be broken, or it might just be very inefficient
- the code is a bit weird because i hastily switched from building off of
  jetforce to writing a purely cgi app. there are also probably some bugs
because of that

### q&a 

*q: why did you write this?*

a: bored.

*q: wait, what is this gemini thing anyway?*

a: gemini is a protocol. https://gemini.circumlunar.space/

*q: how do i pronounce 'motm'*

a: one word like "bottom", or 4 letters like "em-oh-tee-em"

*q: is motm named after the kid cudi album or the counter-strike player?*

a: the [counter-strike player](https://www.hltv.org/player/13230/motm), though
[those
albums](https://en.wikipedia.org/wiki/Kid_Cudi#2009%E2%80%9310:_Man_on_the_Moon_series)
are fantastic, and you should check them out

the first experiment.

(c) 2020 oneseveneight/sose
